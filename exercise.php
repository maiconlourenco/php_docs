<html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/css/exercises.css" />
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <title>Exercises</title>
  </head>
  <body class="exercicio">
    <header class="cabecalho">
      <h1>PHP</h1>
      <h2>Visualização do Exercício</h2>
    </header>
    <nav class="navegacao"> 
    <a href=<?= "/{$_GET['dir']}/{$_GET['file']}.php" ?> class="verde">Sem formatação</a>
    <a href="index.php" class="vermelho">Voltar</a>
   </nav>
    <main class="principal">
      <div class="conteudo">
      <?php
        include("/{$_GET['dir']}/{$_GET['file']}.php");
      ?>
      </div>
    </main>
    <footer class="rodape">MADE BY Maicon Lourenço © <?= date('Y'); ?> </footer>
  </body>
</html>


